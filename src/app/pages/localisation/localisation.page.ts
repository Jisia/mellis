import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';

import {Map , tileLayer , marker , polyline } from "leaflet";
@Component({
  selector: 'app-localisation',
  templateUrl: './localisation.page.html',
  styleUrls: ['./localisation.page.scss'],
})
export class LocalisationPage  {
	map:Map;
	marker:any;
	latlong=[];

  constructor( private geolocation:Geolocation) { }
  ionViewDidEnter(){
  	this.showMap();
  }

  showMap(){
  	this.map=new Map('myMap').setView([-18.8589792,47.5505333],15);
  	tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
    tileSize: 512,
    zoomOffset: -1}).addTo(this.map);
  }
  getPositions(){
  	this.geolocation.getCurrentPosition({
  		enableHighAccuracy: true
  	}).then((res) => {
  		return this.latlong=[
  			res.coords.latitude,
  			res.coords.longitude
  		]
  	}).then((latlng) =>{
  		this.showMarker(latlng)
  	});
  }
  showMarker(latlong){
  	this.marker = marker(latlong);
  	this.marker.addTo(this.map).bindPopup('hey je suis la');
  }
  

}
